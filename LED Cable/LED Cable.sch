EESchema Schematic File Version 4
LIBS:LED Cable-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "LED Cable"
Date "2018-11-07"
Rev "1.0"
Comp "CSUSM Drip-Droppers"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5BCD1F39
P 4700 3900
F 0 "J1" H 4700 4200 50  0000 C CNN
F 1 "TE 172165-1" H 4700 4000 50  0000 C CNN
F 2 "" H 4700 3900 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 172165-1 Diagram.pdf" H 4700 3900 50  0001 C CNN
F 4 "To Power PCB" H 4700 4100 50  0000 C CNN "Name"
	1    4700 3900
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5BCD2A49
P 6350 3950
F 0 "D1" V 6550 3950 50  0000 C CNN
F 1 "Linrose BCMD333UWC-1" V 6300 3850 50  0000 R CNN
F 2 "" H 6350 3950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Linrose BCMD333UWC-1 Datasheet.pdf" H 6350 3950 50  0001 C CNN
F 4 "Optics Illumination, White" V 6400 3850 50  0000 R CNN "Name"
F 5 "3.2V" V 6200 3850 50  0000 R CIN "V_Forward"
F 6 "30mA" V 6200 3550 50  0000 R CIN "I"
	1    6350 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 3900 4900 3900
Text Label 5250 3900 0    50   ~ 0
LED+
Text Label 5250 4000 0    50   ~ 0
GND
Text Notes 4950 3900 0    50   ~ 10
Red
Text Notes 4950 4000 0    50   ~ 10
Black
$Comp
L Device:R_Pack02 RN1
U 1 1 5BCD341F
P 5650 4000
F 0 "RN1" V 5550 3950 50  0001 R CNN
F 1 "Belden 5200FE" V 5450 4000 50  0000 C CNN
F 2 "" V 5690 3990 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Belden 5200FE Datasheet.pdf" H 5650 4000 50  0001 C CNN
F 4 "Sheathed Cable, 140cm" V 5350 4000 50  0000 C CNN "Name"
F 5 "0.01837270341Ω" V 5650 4000 50  0001 C CNN "R"
F 6 "0.64304461942μH" V 5650 4000 50  0001 C CNN "L"
	1    5650 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 3900 6250 3800
Wire Wire Line
	6250 3800 6350 3800
Wire Wire Line
	6350 4100 6250 4100
Wire Wire Line
	6250 4100 6250 4000
Text Notes 4450 3400 0    79   Italic 0
Power connector from Power PCB\nto integrated LED light source
Wire Wire Line
	4900 4000 5450 4000
Wire Wire Line
	5850 3900 6250 3900
Wire Wire Line
	5850 4000 6250 4000
Text Label 6000 4000 0    50   ~ 0
GND
Text Label 6000 3900 0    50   ~ 0
LED+
$Comp
L Connector:Conn_01x01_Female J1.1
U 1 1 5BD0F110
P 4550 3900
F 0 "J1.1" H 4650 3900 50  0000 C CNN
F 1 "TE 1586538-1" H 5350 3900 50  0000 L CNN
F 2 "" H 4550 3900 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1586538-1 Diagram.pdf" H 4550 3900 50  0001 C CNN
F 4 "Socket Crimp" H 4800 3900 50  0000 L CNN "Name"
	1    4550 3900
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J1.2
U 1 1 5BD0F2DD
P 4550 4000
F 0 "J1.2" H 4650 4000 50  0000 C CNN
F 1 "TE 1586538-1" H 5350 4000 50  0000 L CNN
F 2 "" H 4550 4000 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1586538-1 Diagram.pdf" H 4550 4000 50  0001 C CNN
F 4 "Socket Crimp" H 4800 4000 50  0000 L CNN "Name"
	1    4550 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 3900 4900 3900
Connection ~ 4900 3900
Wire Wire Line
	4750 4000 4900 4000
Connection ~ 4900 4000
$EndSCHEMATC
