EESchema Schematic File Version 4
LIBS:Camera IO Cable-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Camera IO Cable"
Date "2018-11-07"
Rev "1.0"
Comp "CSUSM Drip-Droppers"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 5BCDC03A
P 4800 4100
F 0 "J1" H 4800 4600 50  0000 C CNN
F 1 "Alysium A65-3210[1]" H 4900 4000 50  0000 L CNN
F 2 "" H 4800 4100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Alysium A65-321004 Schematic.pdf" H 4800 4100 50  0001 C CNN
F 4 "To Teledyne DALSA Genie Nano GigE" H 4900 4100 50  0000 L CNN "Name"
	1    4800 4100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J2
U 1 1 5BCDC0C7
P 6700 4100
F 0 "J2" H 6700 4800 50  0000 C CNN
F 1 "TE 770580-1" H 6700 4600 50  0000 C CNN
F 2 "" H 6700 4100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 770580-1 Diagram.pdf" H 6700 4100 50  0001 C CNN
F 4 "To Power PCB" H 6700 4700 50  0000 C CNN "Name"
	1    6700 4100
	1    0    0    -1  
$EndComp
Text Notes 6400 3700 2    50   ~ 10
Black
Text Notes 6400 3800 2    50   ~ 10
Red
Text Notes 6400 3900 2    50   ~ 10
Brown
Text Notes 6400 4000 2    50   ~ 10
Orange
Text Notes 6400 4100 2    50   ~ 10
Yellow
Text Notes 6400 4200 2    50   ~ 10
Green
Text Notes 6400 4300 2    50   ~ 10
Blue
Text Notes 6400 4400 2    50   ~ 10
Purple
Text Notes 6400 4500 2    50   ~ 10
Gray
Text Notes 6400 4600 2    50   ~ 10
Silver
Wire Wire Line
	5000 3700 6500 3700
Wire Wire Line
	5000 3800 6500 3800
Wire Wire Line
	5000 3900 6500 3900
Wire Wire Line
	5000 4000 6500 4000
Wire Wire Line
	5000 4100 6500 4100
Wire Wire Line
	5000 4200 6500 4200
Wire Wire Line
	5000 4300 6500 4300
Wire Wire Line
	5000 4400 6500 4400
Wire Wire Line
	5000 4500 6500 4500
Wire Wire Line
	6500 4600 5000 4600
Text Label 5050 3700 0    50   ~ 0
PWR-GND
Text Label 5050 3800 0    50   ~ 0
PWR-VCC
Text Label 5050 3900 0    50   ~ 0
GPI-COMMON
Text Label 5050 4000 0    50   ~ 0
GPO-POWER
Text Label 5050 4100 0    50   ~ 0
GPI1
Text Label 5050 4200 0    50   ~ 0
GPO1
Text Label 5050 4300 0    50   ~ 0
GPI2
Text Label 5050 4400 0    50   ~ 0
GPO2
Text Label 5050 4600 0    50   ~ 0
CHASSIS
Text Label 5050 4500 0    50   Italic 0
Reserved
Text Notes 4800 3150 0    79   Italic 0
1m Cable from Teledyne DALSA\nGenie Nano GigE to Power PCB
Wire Notes Line
	5750 4600 5750 4550
Wire Notes Line
	5700 4550 5700 3650
Wire Notes Line
	5700 3650 5800 3650
Wire Notes Line
	5800 3650 5800 4550
Wire Notes Line
	5700 4550 5800 4550
Text Notes 5450 4700 0    50   Italic 0
Cable Shielding
$Comp
L Connector:Conn_01x01_Female J2.1
U 1 1 5BD0ECAC
P 6850 3700
F 0 "J2.1" H 6900 3700 50  0000 L CNN
F 1 "TE 794058-1" H 7700 3700 50  0000 L CNN
F 2 "" H 6850 3700 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 3700 50  0001 C CNN
F 4 "Socket Crimp" H 7150 3700 50  0000 L CNN "Name"
	1    6850 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.3
U 1 1 5BD0EFA7
P 6850 3900
F 0 "J2.3" H 6900 3900 50  0000 L CNN
F 1 "TE 794058-1" H 7700 3900 50  0000 L CNN
F 2 "" H 6850 3900 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 3900 50  0001 C CNN
F 4 "Socket Crimp" H 7150 3900 50  0000 L CNN "Name"
	1    6850 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.2
U 1 1 5BD0F1EF
P 6850 3800
F 0 "J2.2" H 6900 3800 50  0000 L CNN
F 1 "TE 794058-1" H 7700 3800 50  0000 L CNN
F 2 "" H 6850 3800 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 3800 50  0001 C CNN
F 4 "Socket Crimp" H 7150 3800 50  0000 L CNN "Name"
	1    6850 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3700 6650 3700
Connection ~ 6500 3700
Wire Wire Line
	6500 3800 6650 3800
Connection ~ 6500 3800
Wire Wire Line
	6650 3900 6500 3900
Connection ~ 6500 3900
$Comp
L Connector:Conn_01x01_Female J2.4
U 1 1 5BD0F6EC
P 6850 4000
F 0 "J2.4" H 6900 4000 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4000 50  0000 L CNN
F 2 "" H 6850 4000 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4000 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4000 50  0000 L CNN "Name"
	1    6850 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.5
U 1 1 5BD0F7F0
P 6850 4100
F 0 "J2.5" H 6900 4100 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4100 50  0000 L CNN
F 2 "" H 6850 4100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4100 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4100 50  0000 L CNN "Name"
	1    6850 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.6
U 1 1 5BD0F8F5
P 6850 4200
F 0 "J2.6" H 6900 4200 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4200 50  0000 L CNN
F 2 "" H 6850 4200 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4200 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4200 50  0000 L CNN "Name"
	1    6850 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.7
U 1 1 5BD0F9FB
P 6850 4300
F 0 "J2.7" H 6900 4300 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4300 50  0000 L CNN
F 2 "" H 6850 4300 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4300 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4300 50  0000 L CNN "Name"
	1    6850 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2,8
U 1 1 5BD0FB02
P 6850 4400
F 0 "J2,8" H 6900 4400 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4400 50  0000 L CNN
F 2 "" H 6850 4400 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4400 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4400 50  0000 L CNN "Name"
	1    6850 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.9
U 1 1 5BD0FC0C
P 6850 4500
F 0 "J2.9" H 6900 4500 50  0000 L CNN
F 1 "TE 794058-1" H 7700 4500 50  0000 L CNN
F 2 "" H 6850 4500 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 794058-1 Diagram.pdf" H 6850 4500 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4500 50  0000 L CNN "Name"
	1    6850 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2.10
U 1 1 5BD0FD44
P 6850 4600
F 0 "J2.10" H 6900 4600 50  0000 L CNN
F 1 "TE 1586538-1" H 7700 4600 50  0000 L CNN
F 2 "" H 6850 4600 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1586538-1 Diagram.pdf" H 6850 4600 50  0001 C CNN
F 4 "Socket Crimp" H 7150 4600 50  0000 L CNN "Name"
	1    6850 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4000 6650 4000
Connection ~ 6500 4000
Wire Wire Line
	6500 4100 6650 4100
Connection ~ 6500 4100
Wire Wire Line
	6500 4200 6650 4200
Connection ~ 6500 4200
Wire Wire Line
	6500 4300 6650 4300
Connection ~ 6500 4300
Wire Wire Line
	6500 4400 6650 4400
Connection ~ 6500 4400
Wire Wire Line
	6500 4500 6650 4500
Connection ~ 6500 4500
Wire Wire Line
	6500 4600 6650 4600
Connection ~ 6500 4600
$EndSCHEMATC
