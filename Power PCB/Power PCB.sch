EESchema Schematic File Version 4
LIBS:Power PCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power PCB"
Date "2018-10-24"
Rev "2.0"
Comp "CSUSM Drip-Droppers"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+24V #PWR02
U 1 1 5B9C93B7
P 3650 2450
F 0 "#PWR02" H 3650 2300 50  0001 C CNN
F 1 "+24V" H 3665 2623 50  0000 C CNN
F 2 "" H 3650 2450 50  0001 C CNN
F 3 "" H 3650 2450 50  0001 C CNN
	1    3650 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5B9C99EB
P 3650 2650
F 0 "#PWR03" H 3650 2400 50  0001 C CNN
F 1 "GND" H 3650 2500 50  0000 C CNN
F 2 "" H 3650 2650 50  0001 C CNN
F 3 "" H 3650 2650 50  0001 C CNN
	1    3650 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J2
U 1 1 5B9C9AB8
P 8450 2550
F 0 "J2" H 8450 3050 50  0000 C CNN
F 1 "TE 1-770971-0" H 8550 2500 50  0000 L CNN
F 2 "Project_Footprints:Camera_IO_Connector" H 8450 2550 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1-770971-0 Diagram.pdf" H 8450 2550 50  0001 C CNN
F 4 "To Camera IO Cable" H 8550 2600 50  0000 L CNN "Name"
	1    8450 2550
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5B9C9B45
P 3100 2500
F 0 "J1" H 3100 2600 50  0000 C CNN
F 1 "TE 1-770966-0" H 3200 2400 50  0000 L CNN
F 2 "Project_Footprints:Power_Connector" H 3100 2500 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1-770966-0 Diagram.pdf" H 3100 2500 50  0001 C CNN
F 4 "To Power Supply Out-Cable" H 3200 2500 50  0000 L CNN "Name"
	1    3100 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3650 2500 3650 2450
Text Label 8700 2150 0    50   ~ 0
PWR-GND
$Comp
L power:GND #PWR06
U 1 1 5B9CA9A8
P 9900 3100
F 0 "#PWR06" H 9900 2850 50  0001 C CNN
F 1 "GND" H 9900 2950 50  0000 C CNN
F 2 "" H 9900 3100 50  0001 C CNN
F 3 "" H 9900 3100 50  0001 C CNN
	1    9900 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR05
U 1 1 5B9CA9C5
P 9800 2100
F 0 "#PWR05" H 9800 1950 50  0001 C CNN
F 1 "+24V" H 9815 2273 50  0000 C CNN
F 2 "" H 9800 2100 50  0001 C CNN
F 3 "" H 9800 2100 50  0001 C CNN
	1    9800 2100
	1    0    0    -1  
$EndComp
NoConn ~ 9700 2850
NoConn ~ 9700 2950
Text Label 8700 2250 0    50   ~ 0
PWR-VCC
Text Label 8700 2350 0    50   ~ 0
GPI-COMMON
Text Label 8700 2450 0    50   ~ 0
GPO-POWER
Text Label 8700 2550 0    50   ~ 0
GPI1
Text Label 8700 2650 0    50   ~ 0
GPO1
Text Label 8700 2750 0    50   ~ 0
GPI2
Text Label 8700 2950 0    50   Italic 0
Reserved
Text Label 8700 2850 0    50   ~ 0
GPO2
Text Label 8700 3050 0    50   ~ 0
CHASSIS
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5B9D099D
P 8850 5000
F 0 "#FLG0101" H 8850 5075 50  0001 C CNN
F 1 "PWR_FLAG" H 8850 5174 50  0000 C CNN
F 2 "" H 8850 5000 50  0001 C CNN
F 3 "~" H 8850 5000 50  0001 C CNN
	1    8850 5000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5B9D0D96
P 8850 4500
F 0 "#FLG0102" H 8850 4575 50  0001 C CNN
F 1 "PWR_FLAG" H 8850 4673 50  0000 C CNN
F 2 "" H 8850 4500 50  0001 C CNN
F 3 "~" H 8850 4500 50  0001 C CNN
	1    8850 4500
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR0101
U 1 1 5B9D116F
P 8850 4450
F 0 "#PWR0101" H 8850 4300 50  0001 C CNN
F 1 "+24V" H 8865 4623 50  0000 C CNN
F 2 "" H 8850 4450 50  0001 C CNN
F 3 "" H 8850 4450 50  0001 C CNN
	1    8850 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5B9D1180
P 8850 5050
F 0 "#PWR0102" H 8850 4800 50  0001 C CNN
F 1 "GND" H 8855 4877 50  0000 C CNN
F 2 "" H 8850 5050 50  0001 C CNN
F 3 "" H 8850 5050 50  0001 C CNN
	1    8850 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 4450 8850 4500
Wire Wire Line
	8850 5000 8850 5050
Text Notes 8100 4000 0    79   Italic 0
Mark power lines as such for\nthe electrical rules checker
Text Notes 2100 1650 0    79   Italic 0
Power cable to 24V supply
Text Notes 8050 1650 0    79   Italic 0
"Alysium A65-321004" cable\nattached via 2x5 connector
$Comp
L Mechanical:MountingHole MH1
U 1 1 5BAEB40F
P 2450 4250
F 0 "MH1" H 2550 4296 50  0000 L CNN
F 1 "⌀6.5mm" H 2550 4205 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_0.25in_Through" H 2450 4250 50  0001 C CNN
F 3 "~" H 2450 4250 50  0001 C CNN
	1    2450 4250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5BAEB575
P 2450 4450
F 0 "MH2" H 2550 4496 50  0000 L CNN
F 1 "⌀6.5mm" H 2550 4405 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_0.25in_Through" H 2450 4450 50  0001 C CNN
F 3 "~" H 2450 4450 50  0001 C CNN
	1    2450 4450
	1    0    0    -1  
$EndComp
Text Notes 9300 3050 0    39   ~ 8
Silver
Text Notes 9300 2950 0    39   ~ 8
Gray
Text Notes 9300 2850 0    39   ~ 8
Purple
Text Notes 9300 2750 0    39   ~ 8
Blue
Text Notes 9300 2650 0    39   ~ 8
Green
Text Notes 9300 2550 0    39   ~ 8
Yellow
Text Notes 9300 2450 0    39   ~ 8
Orange
Text Notes 9300 2350 0    39   ~ 8
Brown
Text Notes 9300 2250 0    39   ~ 8
Red
Text Notes 9300 2150 0    39   ~ 8
Black
$Comp
L Mechanical:MountingHole MH3
U 1 1 5BB84CD8
P 2450 4650
F 0 "MH3" H 2550 4696 50  0000 L CNN
F 1 "⌀6.5mm" H 2550 4605 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_0.25in_Through" H 2450 4650 50  0001 C CNN
F 3 "~" H 2450 4650 50  0001 C CNN
	1    2450 4650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5BB84CDF
P 2450 4850
F 0 "MH4" H 2550 4896 50  0000 L CNN
F 1 "⌀6.5mm" H 2550 4805 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_0.25in_Through" H 2450 4850 50  0001 C CNN
F 3 "~" H 2450 4850 50  0001 C CNN
	1    2450 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5BBB2584
P 6150 2500
F 0 "J3" H 6150 2600 50  0000 C CNN
F 1 "TE 1-770966-0" H 6250 2400 50  0000 L CNN
F 2 "Project_Footprints:Power_Connector" H 6150 2500 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1-770966-0 Diagram.pdf" H 6150 2500 50  0001 C CNN
F 4 "To LED Cable" H 6250 2500 50  0000 L CNN "Name"
	1    6150 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 5BBBA5FF
P 4800 2200
F 0 "R1" H 4900 2300 50  0000 L CNN
F 1 "Vishay RS01A700R0FB12" H 4900 2100 50  0000 L CNN
F 2 "Project_Footprints:LED_Power_Resistor" V 4840 2190 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Vishay RS01A700R0FB12 Datasheet.pdf" H 4800 2200 50  0001 C CNN
F 4 "700Ω 1W 1%" H 4900 2200 50  0000 L CNN "Name"
	1    4800 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5BBBE277
P 4800 2650
F 0 "#PWR08" H 4800 2400 50  0001 C CNN
F 1 "GND" H 4805 2477 50  0000 C CNN
F 2 "" H 4800 2650 50  0001 C CNN
F 3 "" H 4800 2650 50  0001 C CNN
	1    4800 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR07
U 1 1 5BBC347A
P 4800 2000
F 0 "#PWR07" H 4800 1850 50  0001 C CNN
F 1 "+24V" H 4815 2173 50  0000 C CNN
F 2 "" H 4800 2000 50  0001 C CNN
F 3 "" H 4800 2000 50  0001 C CNN
	1    4800 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2000 4800 2050
Text Notes 4750 1650 0    79   Italic 0
Power cable to LED light source
Wire Wire Line
	4800 2500 5950 2500
Wire Wire Line
	4800 2600 5950 2600
Text Label 5350 2500 0    50   ~ 0
LED+
Text Notes 5950 2500 2    39   ~ 8
Red
Text Notes 5950 2600 2    39   ~ 8
Black
$Comp
L Mechanical:MountingHole MH5
U 1 1 5BC15CAB
P 5350 4250
F 0 "MH5" V 5300 4350 50  0000 L CNN
F 1 "⌀3.2mm" V 5400 4350 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_M3_Standoff" H 5350 4250 50  0001 C CNN
F 3 "~" H 5350 4250 50  0001 C CNN
	1    5350 4250
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MH6
U 1 1 5BC15DF7
P 5350 4450
F 0 "MH6" V 5300 4550 50  0000 L CNN
F 1 "⌀3.2mm" V 5400 4550 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_M3_Standoff" H 5350 4450 50  0001 C CNN
F 3 "~" H 5350 4450 50  0001 C CNN
	1    5350 4450
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MH7
U 1 1 5BC163B8
P 5350 4650
F 0 "MH7" V 5300 4750 50  0000 L CNN
F 1 "⌀3.2mm" V 5400 4750 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_M3_Standoff" H 5350 4650 50  0001 C CNN
F 3 "~" H 5350 4650 50  0001 C CNN
	1    5350 4650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MH8
U 1 1 5BC1697A
P 5350 4850
F 0 "MH8" V 5300 4950 50  0000 L CNN
F 1 "⌀3.2mm" V 5400 4950 50  0000 L CNN
F 2 "Project_Footprints:MountingHole_M3_Standoff" H 5350 4850 50  0001 C CNN
F 3 "~" H 5350 4850 50  0001 C CNN
	1    5350 4850
	0    1    1    0   
$EndComp
Text Notes 2050 4000 0    79   Italic 0
1/4" mounts through board\nand case into optics table
Text Notes 5000 4000 0    79   Italic 0
M3 mounts for spacers\nto acrylic case plates
Text Label 3300 2600 0    50   ~ 0
GND
Text Label 3300 2500 0    50   ~ 0
+24V
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5BC3256E
P 9300 5000
F 0 "#FLG0103" H 9300 5075 50  0001 C CNN
F 1 "PWR_FLAG" H 9300 5174 50  0000 C CNN
F 2 "" H 9300 5000 50  0001 C CNN
F 3 "~" H 9300 5000 50  0001 C CNN
	1    9300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5000 9300 5050
$Comp
L power:Earth #PWR0103
U 1 1 5BC3312D
P 9300 5050
F 0 "#PWR0103" H 9300 4800 50  0001 C CNN
F 1 "Earth" H 9300 4900 50  0000 C CNN
F 2 "" H 9300 5050 50  0001 C CNN
F 3 "~" H 9300 5050 50  0001 C CNN
	1    9300 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK5
U 1 1 5BCE3FE6
P 5350 5900
F 0 "MK5" V 5259 6049 50  0001 L CNN
F 1 "RAF M1311-3005-AL" V 5400 6000 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Front" H 5350 5900 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M1311-3005-AL Diagram.pdf" H 5350 5900 50  0001 C CNN
F 4 "Female Standoff, 14mm" V 5300 6000 50  0000 L CNN "Name"
	1    5350 5900
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK6
U 1 1 5BCE7D7E
P 5350 6100
F 0 "MK6" V 5259 6249 50  0001 L CNN
F 1 "RAF M1311-3005-AL" V 5400 6200 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Front" H 5350 6100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M1311-3005-AL Diagram.pdf" H 5350 6100 50  0001 C CNN
F 4 "Female Standoff, 14mm" V 5300 6200 50  0000 L CNN "Name"
	1    5350 6100
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK7
U 1 1 5BCE85A1
P 5350 6300
F 0 "MK7" V 5259 6449 50  0001 L CNN
F 1 "RAF M1311-3005-AL" V 5400 6400 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Front" H 5350 6300 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M1311-3005-AL Diagram.pdf" H 5350 6300 50  0001 C CNN
F 4 "Female Standoff, 14mm" V 5300 6400 50  0000 L CNN "Name"
	1    5350 6300
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK8
U 1 1 5BCE8DC4
P 5350 6500
F 0 "MK8" V 5259 6649 50  0001 L CNN
F 1 "RAF M1311-3005-AL" V 5400 6600 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Front" H 5350 6500 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M1311-3005-AL Diagram.pdf" H 5350 6500 50  0001 C CNN
F 4 "Female Standoff, 14mm" V 5300 6600 50  0000 L CNN "Name"
	1    5350 6500
	0    1    1    0   
$EndComp
Text Notes 5000 5650 0    79   Italic 0
Front M3 standoffs\nto acrylic case plate
$Comp
L Mechanical:MountingHole MK1
U 1 1 5BCECF49
P 2450 5900
F 0 "MK1" V 2359 6049 50  0001 L CNN
F 1 "RAF M2141-3005-AL" V 2500 6000 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Back" H 2450 5900 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M2141-3005-AL Diagram.pdf" H 2450 5900 50  0001 C CNN
F 4 "Male/Female Standoff, 6mm" V 2400 6000 50  0000 L CNN "Name"
	1    2450 5900
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK2
U 1 1 5BCECF50
P 2450 6100
F 0 "MK2" V 2359 6249 50  0001 L CNN
F 1 "RAF M2141-3005-AL" V 2500 6200 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Back" H 2450 6100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M2141-3005-AL Diagram.pdf" H 2450 6100 50  0001 C CNN
F 4 "Male/Female Standoff, 6mm" V 2400 6200 50  0000 L CNN "Name"
	1    2450 6100
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK3
U 1 1 5BCECF57
P 2450 6300
F 0 "MK3" V 2359 6449 50  0001 L CNN
F 1 "RAF M2141-3005-AL" V 2500 6400 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Back" H 2450 6300 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M2141-3005-AL Diagram.pdf" H 2450 6300 50  0001 C CNN
F 4 "Male/Female Standoff, 6mm" V 2400 6400 50  0000 L CNN "Name"
	1    2450 6300
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole MK4
U 1 1 5BCECF5E
P 2450 6500
F 0 "MK4" V 2359 6649 50  0001 L CNN
F 1 "RAF M2141-3005-AL" V 2500 6600 50  0000 L CNN
F 2 "Project_Footprints:Standoff_M3_Back" H 2450 6500 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/RAF M2141-3005-AL Diagram.pdf" H 2450 6500 50  0001 C CNN
F 4 "Male/Female Standoff, 6mm" V 2400 6600 50  0000 L CNN "Name"
	1    2450 6500
	0    1    1    0   
$EndComp
Text Notes 2100 5650 0    79   Italic 0
Back M3 standoff spacers\nto acrylic case plate
Wire Wire Line
	4800 2350 4800 2500
Wire Wire Line
	9800 2100 9800 2250
Wire Wire Line
	9900 2150 9900 2350
Wire Wire Line
	8650 2950 9700 2950
Wire Wire Line
	8650 2850 9700 2850
Wire Wire Line
	8650 2750 9900 2750
Connection ~ 9900 2750
Wire Wire Line
	9900 2750 9900 3050
Wire Wire Line
	8650 2650 9700 2650
NoConn ~ 9700 2650
Wire Wire Line
	8650 2550 9900 2550
Connection ~ 9900 2550
Wire Wire Line
	9900 2550 9900 2750
Wire Wire Line
	8650 2450 9800 2450
Wire Wire Line
	8650 2350 9900 2350
Connection ~ 9900 2350
Wire Wire Line
	9900 2350 9900 2550
Wire Wire Line
	8650 2150 9900 2150
Wire Wire Line
	8650 2250 9800 2250
Connection ~ 9800 2250
Wire Wire Line
	9800 2250 9800 2450
Wire Wire Line
	3300 2600 3650 2600
Wire Wire Line
	3650 2600 3650 2650
Wire Wire Line
	3300 2500 3650 2500
Connection ~ 9900 3050
Wire Wire Line
	9900 3050 9900 3100
Wire Wire Line
	8650 3050 9900 3050
Wire Wire Line
	4800 2650 4800 2600
$EndSCHEMATC
