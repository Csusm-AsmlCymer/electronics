EESchema Schematic File Version 4
LIBS:Power Supply Cables-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Supply Cables"
Date "2018-11-07"
Rev "1.0"
Comp "CSUSM Drip-Droppers"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x07 J4
U 1 1 5BD120FE
P 6100 4450
F 0 "J4" V 6100 4850 50  0001 C CNN
F 1 "Screw_Terminal_01x07" H 6200 4400 50  0001 L CNN
F 2 "" H 6100 4450 50  0001 C CNN
F 3 "~" H 6100 4450 50  0001 C CNN
F 4 "Power Supply Barrier Terminal Block" V 6200 4450 50  0000 C CNN "Name"
	1    6100 4450
	0    -1   1    0   
$EndComp
$Comp
L Converter_ACDC:VTX-214-015-124 PS1
U 1 1 5BD120FF
P 6100 3750
F 0 "PS1" H 6100 4200 50  0000 C CNN
F 1 "MEAN WELL SP-200-24" H 6100 4000 50  0000 C CNN
F 2 "" H 6100 4100 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/MEAN WELL SP-200-24 Datasheet.pdf" H 7200 2950 50  0001 C CNN
F 4 "Power Supply" H 6100 4100 50  0000 C CNN "Name"
	1    6100 3750
	1    0    0    -1  
$EndComp
Text Label 5700 3650 2    50   ~ 0
AC(LOAD)
Text Label 5700 3850 2    50   ~ 0
AC(NEUT)
Text Label 6500 3650 0    50   ~ 0
24V
Text Label 6500 3850 0    50   ~ 0
GND
$Comp
L power:NEUT #PWR05
U 1 1 5BD12100
P 5150 5800
F 0 "#PWR05" H 5150 5650 50  0001 C CNN
F 1 "NEUT" H 5150 6000 50  0000 C CNN
F 2 "" H 5150 5800 50  0001 C CNN
F 3 "" H 5150 5800 50  0001 C CNN
	1    5150 5800
	-1   0    0    1   
$EndComp
$Comp
L power:AC #PWR04
U 1 1 5BCD9E61
P 5150 5500
F 0 "#PWR04" H 5150 5400 50  0001 C CNN
F 1 "AC" H 5150 5775 50  0000 C CNN
F 2 "" H 5150 5500 50  0001 C CNN
F 3 "" H 5150 5500 50  0001 C CNN
	1    5150 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5BCD64FC
P 8200 5550
F 0 "J3" H 8200 5850 50  0000 C CNN
F 1 "TE 172165-1" H 8200 5650 50  0000 C CNN
F 2 "" H 8200 5550 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 172165-1 Diagram.pdf" H 8200 5550 50  0001 C CNN
F 4 "To Power PCB" H 8200 5750 50  0000 C CNN "Name"
	1    8200 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2.2
U 1 1 5BCD6745
P 6400 4950
F 0 "J2.2" H 6450 4950 50  0000 L CNN
F 1 "Keystone 8203" V 6400 5050 50  0000 L CNN
F 2 "" H 6400 4950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Keystone 8203 Diagram.pdf" H 6400 4950 50  0001 C CNN
F 4 "+V" V 6650 4950 50  0000 C CNN "Name"
	1    6400 4950
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2.1
U 1 1 5BCD67E9
P 6200 4950
F 0 "J2.1" H 6250 4950 50  0000 L CNN
F 1 "Keystone 8203" H 6400 4950 50  0001 L CNN
F 2 "" H 6200 4950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Keystone 8203 Diagram.pdf" H 6200 4950 50  0001 C CNN
F 4 "-V" V 6450 4950 50  0000 C CNN "Name"
	1    6200 4950
	0    1    -1   0   
$EndComp
Text Label 6950 5550 0    50   ~ 0
24V
Text Label 6950 5650 0    50   ~ 0
GND
Text Notes 6550 5550 0    50   ~ 10
Red
Text Notes 6550 5650 0    50   ~ 10
Black
$Comp
L Device:R_Pack02 RN1
U 1 1 5BD0AFA3
P 7350 5650
F 0 "RN1" V 7033 5650 50  0001 C CNN
F 1 "Belden 5200FE" V 7150 5650 50  0000 C CNN
F 2 "" V 7525 5650 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Belden 5200FE Datasheet.pdf" H 7350 5650 50  0001 C CNN
F 4 "Sheathed Cable, 80cm" V 7450 5650 50  0000 C CNN "Name"
F 5 "0.01049868766Ω" V 7350 5650 50  0001 C CNN "R"
F 6 "0.36745406824μH" V 7350 5650 50  0001 C CNN "L"
	1    7350 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 5550 8000 5550
Text Label 7600 5650 0    50   ~ 0
GND
Text Label 7600 5550 0    50   ~ 0
24V
Text Notes 7150 5100 0    79   Italic 0
Power Supply Out-Cable with\n24V DC to Power PCB
$Comp
L Connector:Conn_WallPlug_Earth P1
U 1 1 5BCD84FE
P 4250 5650
F 0 "P1" H 4200 5850 50  0000 C CNN
F 1 "Hanvex H3S14AWG" H 4050 5600 50  0000 R CNN
F 2 "" H 4650 5650 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Hanvex H3S14AWG Datasheet.pdf" H 4650 5650 50  0001 C CNN
F 4 "NEMA-5-15 Plug, 6ft, Exposed Leads" H 4050 5700 50  0000 R CNN "Name"
	1    4250 5650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1.1
U 1 1 5BCD8FAB
P 5800 4950
F 0 "J1.1" H 5850 4950 50  0000 L CNN
F 1 "Keystone 8203" V 5800 5050 50  0000 L CNN
F 2 "" H 5800 4950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Keystone 8203 Diagram.pdf" H 5800 4950 50  0001 C CNN
F 4 "L" V 6050 4950 50  0000 C CNN "Name"
	1    5800 4950
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1.2
U 1 1 5BCD9104
P 5900 4950
F 0 "J1.2" H 5950 4950 50  0000 L CNN
F 1 "Keystone 8203" H 5980 4901 50  0001 L CNN
F 2 "" H 5900 4950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Keystone 8203 Diagram.pdf" H 5900 4950 50  0001 C CNN
F 4 "N" V 6150 4950 50  0000 C CNN "Name"
	1    5900 4950
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1.3
U 1 1 5BCD9241
P 6000 4950
F 0 "J1.3" H 6050 4950 50  0000 L CNN
F 1 "Keystone 8203" H 6080 4901 50  0001 L CNN
F 2 "" H 6000 4950 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/Keystone 8203 Diagram.pdf" H 6000 4950 50  0001 C CNN
F 4 "⏚" V 6250 4950 50  0000 C CNN "Name"
	1    6000 4950
	0    -1   -1   0   
$EndComp
Text Label 4700 5550 0    50   ~ 0
AC(LOAD)
Text Label 4700 5650 0    50   ~ 0
AC(NEUT)
Text Label 4700 5750 0    50   ~ 0
Earth
Wire Wire Line
	5800 5550 5800 5150
Wire Wire Line
	5900 5650 5900 5150
Wire Wire Line
	6000 5750 6000 5150
Text Notes 5250 5550 0    50   ~ 10
Black
Text Notes 5250 5650 0    50   ~ 10
White
Text Notes 5250 5750 0    50   ~ 10
Green/Yellow
Wire Wire Line
	4550 5750 4550 5650
Wire Wire Line
	4550 5950 4650 5950
Wire Wire Line
	4650 5950 4650 5750
Text Notes 4700 5100 2    79   Italic 0
Power Supply In-Cable from\nU.S. 120V 60Hz AC, max 15A
Wire Wire Line
	4550 5550 5150 5550
Wire Wire Line
	4550 5650 5150 5650
Wire Wire Line
	4650 5750 4900 5750
Wire Wire Line
	5150 5500 5150 5550
Connection ~ 5150 5550
Wire Wire Line
	5150 5550 5800 5550
Wire Wire Line
	5150 5800 5150 5650
Connection ~ 5150 5650
Wire Wire Line
	5150 5650 5900 5650
$Comp
L power:+24V #PWR06
U 1 1 5BD261F1
P 6850 5500
F 0 "#PWR06" H 6850 5350 50  0001 C CNN
F 1 "+24V" H 6865 5673 50  0000 C CNN
F 2 "" H 6850 5500 50  0001 C CNN
F 3 "" H 6850 5500 50  0001 C CNN
	1    6850 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5BD26256
P 6850 5700
F 0 "#PWR07" H 6850 5450 50  0001 C CNN
F 1 "GND" H 6855 5527 50  0000 C CNN
F 2 "" H 6850 5700 50  0001 C CNN
F 3 "" H 6850 5700 50  0001 C CNN
	1    6850 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5650 6850 5700
Connection ~ 6850 5650
Wire Wire Line
	6850 5650 7150 5650
Wire Wire Line
	6850 5550 6850 5500
Connection ~ 6850 5550
Wire Wire Line
	6850 5550 7150 5550
Wire Wire Line
	5800 4250 5600 4250
Wire Wire Line
	5600 4250 5600 3650
Wire Wire Line
	5600 3650 5700 3650
Wire Wire Line
	5900 4250 5900 4150
Wire Wire Line
	5900 4150 5700 4150
Wire Wire Line
	5700 4150 5700 3850
Wire Wire Line
	6100 4250 6200 4250
Wire Wire Line
	6300 4250 6400 4250
Wire Wire Line
	6200 4250 6200 4150
Wire Wire Line
	6200 4150 6500 4150
Wire Wire Line
	6500 4150 6500 3850
Connection ~ 6200 4250
Wire Wire Line
	6400 4250 6600 4250
Wire Wire Line
	6600 4250 6600 3650
Wire Wire Line
	6600 3650 6500 3650
Connection ~ 6400 4250
Wire Wire Line
	6000 4050 6100 4050
Wire Wire Line
	6000 4050 6000 4250
Text Label 6000 4050 2    50   ~ 0
Earth
Wire Wire Line
	6400 5550 6850 5550
Wire Wire Line
	6400 5150 6400 5550
Wire Wire Line
	6200 5650 6200 5150
Wire Wire Line
	6200 5650 6850 5650
Wire Notes Line
	6450 4450 6900 4450
Wire Notes Line
	6900 3450 5300 3450
Wire Notes Line
	5300 4450 5750 4450
Wire Notes Line
	6900 3450 6900 4450
Wire Notes Line
	5300 4450 5300 3450
Text Notes 2200 3050 0    79   Italic 0
Mark power lines as such for\nthe electrical rules checker
$Comp
L power:Earth #PWR0101
U 1 1 5BD3C5CE
P 3050 3550
F 0 "#PWR0101" H 3050 3300 50  0001 C CNN
F 1 "Earth" H 3050 3350 50  0000 C CNN
F 2 "" H 3050 3550 50  0001 C CNN
F 3 "~" H 3050 3550 50  0001 C CNN
	1    3050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5800 4900 5750
Connection ~ 4900 5750
Wire Wire Line
	4900 5750 6000 5750
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5BD3D4FC
P 3050 3500
F 0 "#FLG0101" H 3050 3575 50  0001 C CNN
F 1 "PWR_FLAG" H 3050 3674 50  0000 C CNN
F 2 "" H 3050 3500 50  0001 C CNN
F 3 "~" H 3050 3500 50  0001 C CNN
	1    3050 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3550 3050 3500
$Comp
L power:Earth #PWR0102
U 1 1 5BD3DC5D
P 4900 5800
F 0 "#PWR0102" H 4900 5550 50  0001 C CNN
F 1 "Earth" H 4900 5600 50  0000 C CNN
F 2 "" H 4900 5800 50  0001 C CNN
F 3 "~" H 4900 5800 50  0001 C CNN
	1    4900 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5650 7550 5650
$Comp
L Connector:Conn_01x01_Female J3.1
U 1 1 5BD1057E
P 8350 5550
F 0 "J3.1" H 8400 5550 50  0000 L CNN
F 1 "TE 1586538-1" H 9150 5550 50  0000 L CNN
F 2 "" H 8350 5550 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1586538-1 Diagram.pdf" H 8350 5550 50  0001 C CNN
F 4 "Socket Crimp" H 8600 5550 50  0000 L CNN "Name"
	1    8350 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J3.2
U 1 1 5BD117C5
P 8350 5650
F 0 "J3.2" H 8400 5650 50  0000 L CNN
F 1 "TE 1586538-1" H 9150 5650 50  0000 L CNN
F 2 "" H 8350 5650 50  0001 C CNN
F 3 "%KIPRJMOD%/../Datasheets/TE 1586538-1 Diagram.pdf" H 8350 5650 50  0001 C CNN
F 4 "Socket Crimp" H 8600 5650 50  0000 L CNN "Name"
	1    8350 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5650 8150 5650
Connection ~ 8000 5650
Wire Wire Line
	8000 5550 8150 5550
Connection ~ 8000 5550
$EndSCHEMATC
