# Drip Dropper - [Electronics](https://bitbucket.org/Csusm-AsmlCymer/electronics/)

A custom printed circuit board (PCB) designed using [KiCad](http://kicad-pcb.org/),
along with schematics for cabling and power supply connections.
This circuit powers the [Teledyne DALSA Genie Nano GigE camera](
http://www.teledynedalsa.com/en/products/imaging/cameras/genie-nano-gige/) and
an LED light source with a 24V power supply.

For a quick guide to KiCad, check out its guide, [Getting Started
in KiCad 5.0](http://docs.kicad-pcb.org/5.0.0/en/getting_started_in_kicad.html).

![3D render of the PCB](./Power%20PCB/Board%20Render.jpg "A ray-traced 3D rendering of the PCB.")


## Assembly

For a Bill of Materials (BOM), see the Camera Electronics tab of the 
[`Parts/Bill of Materials` spreadsheet](
https://docs.google.com/spreadsheets/d/1XbXHplOWNEAyq9XfQIm8a0ZQZ8VbA_nUQcGwmKp6frE/edit?usp=sharing)
shared in the project Google Drive folder.

### Power PCB

*   [Electrical schematic PDF](./Power%20PCB/Schematic.pdf)
*   [Front and back PCB schematic PDF](./Power%20PCB/Board.pdf)
*   [Board layout file for OSHPark PCB manufacturer](./Power%20PCB/Power%20PCB.kicad_pcb)
*   [Acrylic case (front/top) DXF file for laser cutter](./Power%20PCB/Laser-Cut Cover Front.dxf)
*   [Acrylic case (back/bottom) DXF file for laser cutter](./Power%20PCB/Laser-Cut Cover Back.dxf)

This board design can be fabricated from any number of companies. We used [OSHPark](
https://docs.oshpark.com/services/) based in the US. Check out their webpage for
instructions on ordering PCBs.

Connectors and an LED's current-limiting resistor from the BOM must be soldered
onto the PCB. Hexagonal headers screw through the smaller M3 mounting holes,
extending from both the top and bottom of the board to attach to laser-cut
acrylic cover plates with bolts. 1/4-inch-20 bolts then attach the encased PCB
to an optics table with a 1-inch grid of holes.

### Power Supply Cables

*   [Electrical schematic PDF](./Power%20Supply%20Cables/Schematic.pdf)

#### In-Cable

This cable consists of a pre-made 3-prong North American [NEMA-5-15 plug](
https://en.wikipedia.org/wiki/NEMA_connector#NEMA_5) and cable with exposed leads,
which must be crimped with individual ring-terminal connectors to be screwed into
the [power supply](./Datasheets/MEAN%20WELL%20SP-200-24%20Datasheet.pdf)'s `N`
(AC Neurtral, white), `L` (AC Load, black), and `⏚` (Earth, green/yellow)
terminals.

#### Out-Cable

This cable connects the [power supply](./Datasheets/MEAN%20WELL%20SP-200-24%20Datasheet.pdf)'s
`+V` and `-V` terminals with ring-terminal connectors, crimped to red and black
wires respectively. At the other end of the cable, these wires are crimped and
stuffed into a [female 2-pin TE Universal Mini MATE-N-LOK connector](
./Datasheets/TE%20172165-1%20Diagram.pdf) that attaches to the Power PCB.

### Camera IO Cable

*   [Electrical schematic PDF](./Camera%20IO%20Cable/Schematic.pdf)

This cable is assembled based on a pre-made [Alysium A65-3210](
./Datasheets/Alysium%20A65-321004%20Schematic.pdf) cable with a 10-pin connector
to the camera and free leads on the other end. These free leads must be crimped
and stuffed into a [female 10-pin Universal Mini MATE-N-LOK connector](
./Datasheets/TE%20770580-1%20Diagram.pdf) for connecting to the Power PCB.

### LED Cable

*   [Electrical schematic PDF](./LED%20Cable/Schematic.pdf)

This cable connects the Power PCB directly to an integrated illuminating LED for
the camera. At the Power PCB end, this two-signal red (`LED+`) and black (`GND`)
bonded cable is crimped and stuffed into a [female 2-pin TE MATE-N-LOK connector](
./Datasheets/TE%20172165-1%20Diagram.pdf). At the other end, this cable is
directly soldered to a Linrose BCMD204UWC LED.
